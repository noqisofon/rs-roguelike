use std::cmp::{max, min};
use rltk::{Rltk, GameState, RGB, VirtualKeyCode};
use specs::prelude::*;
use specs_derive::Component;

struct State {}
impl GameState for State {
    fn tick(&mut self, context : &mut Rltk) {
        context.cls();
        context.print( 1, 1, "Hello Rust World" );
    }
}

#[derive(Component)]
struct Position {
    x: i32,
    y: i32,
}

#[derive(Component)]
struct Renderable {
    glyph: rltk::FontCharType,
    fg: RGB,
    bg: RGB
}

fn main() -> rltk::BError {
    use rltk::RltkBuilder;

    let context = RltkBuilder::simple80x50()
        .with_title( "Roguelike Tutorial" )
        .build()?;

    let gs = State {};
    rltk::main_loop( context, gs )
}
